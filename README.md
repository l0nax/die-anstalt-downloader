# "Die Anstalt" Downloader

This is a simple side-project which downloads all the videos of [Die Anstalt](https://de.wikipedia.org/wiki/Die_Anstalt).

## Motivation

Preservation of the beautiful content.

## Technical Details

To keep the project _real_ simple, the RSS API of https://mediathekviewweb.de/ (GitHub: https://github.com/mediathekview/mediathekviewweb)
is downloaded and parsed.
