package main

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/mmcdole/gofeed"
	"github.com/pkg/errors"
)

func init() {
	// NOTE: We do NOT need real randomness
	rand.Seed(time.Now().UnixNano())
}

var c *resty.Client

func main() {
	fp := gofeed.NewParser()

	// Search Query
	//  !zdf Die Anstalt >20
	feed, err := fp.ParseURL("https://mediathekviewweb.de/feed?query=!zdf%20Die%20Anstalt%20%3E20&everywhere=true&future=false")
	if err != nil {
		log.Fatalf("Unable to download RSS Feed: %+v\n", err)
		os.Exit(1)
	}

	c = resty.New()

	if err := os.MkdirAll("./out/", 0700); err != nil {
		log.Fatalf("Unable to create local outpu directory: %+v\n", err)
		os.Exit(1)
	}

	if err := parseItems(feed.Items); err != nil {
		log.Fatalf("Unable to parse Feed Items: %+v\n", err)
		os.Exit(1)
	}

	log.Println("Successfully parsed all data!")
}

func parseItems(items []*gofeed.Item) error {
	for i := range items {
		for _, enc := range items[i].Enclosures {
			if enc.Type != "video/mp4" {
				continue
			}

			if err := processEnclosure(items[i], enc); err != nil {
				return errors.Wrap(err, "unable to process enclosure")
			}
		}

	}

	return nil
}

func processEnclosure(i *gofeed.Item, e *gofeed.Enclosure) error {
	// To have the full title in Jellyfin, we have to name the file after the title...
	name := fmt.Sprintf("./Die Anstalt/%s.mp4", i.Title)

	fmt.Printf("[+] Start download of %q from %q\n", "Die Anstalt", i.PublishedParsed.String())

	if fileExists(name) {
		log.Printf("[+] File already downloaded: skipping...\n")
		return nil
	}

	rand := RandStringRunes(16)
	tmpPath := filepath.Join("/tmp/", rand)

	_, err := c.R().
		SetOutput(tmpPath).
		Get(e.URL)
	if err != nil {
		return errors.Wrapf(err, "unable to download file from %q", e.URL)
	}

	if err := CopyFile(tmpPath, name); err != nil {
		log.Fatalf("[!] Unable to move temporary file from %q to %q: %+v\n", tmpPath, name, err)
		os.Exit(1)
	}

	if err := os.Remove(tmpPath); err != nil {
		log.Printf("[!] Unable to remove temporary file %q: %+v\n", tmpPath, err)
	}

	log.Printf("Downloaded from %s\n", i.PublishedParsed.String())

	log.Printf("[+] Finished Processing of entry from %q", i.PublishedParsed.String())

	return nil
}

func fileExists(path string) bool {
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		return false
	} else if err != nil {
		log.Printf("[!] An unexpected error occured while checking the existence of %q: %+v\n", path, err)
	}

	return true
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// CopyFile copies a file from src to dst. If src and dst files exist, and are
// the same, then return success. Otherise, attempt to create a hard link
// between the two files. If that fail, copy the file contents from src to dst.
//
// NOTE: Copied from https://stackoverflow.com/a/21067803
func CopyFile(src, dst string) (err error) {
	sfi, err := os.Stat(src)
	if err != nil {
		return
	}
	if !sfi.Mode().IsRegular() {
		// cannot copy non-regular files (e.g., directories,
		// symlinks, devices, etc.)
		return fmt.Errorf("CopyFile: non-regular source file %s (%q)", sfi.Name(), sfi.Mode().String())
	}
	dfi, err := os.Stat(dst)
	if err != nil {
		if !os.IsNotExist(err) {
			return
		}
	} else {
		if !(dfi.Mode().IsRegular()) {
			return fmt.Errorf("CopyFile: non-regular destination file %s (%q)", dfi.Name(), dfi.Mode().String())
		}
		if os.SameFile(sfi, dfi) {
			return
		}
	}
	if err = os.Link(src, dst); err == nil {
		return
	}
	err = copyFileContents(src, dst)
	return
}

// copyFileContents copies the contents of the file named src to the file named
// by dst. The file will be created if it does not already exist. If the
// destination file exists, all it's contents will be replaced by the contents
// of the source file.
//
// NOTE: Copied from https://stackoverflow.com/a/21067803
func copyFileContents(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()
	if _, err = io.Copy(out, in); err != nil {
		return
	}
	err = out.Sync()
	return
}
